import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { BarcodeScanner } from '@capacitor-community/barcode-scanner';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.page.html',
  styleUrls: ['./principal.page.scss'],
})
export class PrincipalPage implements OnInit {

  mdl_name: string = '';
  mdl_lastname: string = '';
  mdl_correo: string = '';
  texto: string='';
  user_ini = [];
  texto2 = [];
  asistencia: string='';


  constructor(private router: Router, private api: ApiService, private loading: LoadingController,
              private toastController: ToastController) { 

              const user1 = JSON.parse(localStorage.getItem('usuario'));
              this.mdl_correo = user1.user

    let that = this;

    that.api.usuarioObtenerNombre().then((data:any) => {
      console.log(data);
      that.mdl_name = data.result[0].NOMBRE; 
      that.mdl_lastname = data.result[0].APELLIDO;
    })  
  }

  ngOnInit() {
    this.loading.create({
      message: 'Obteniendo Información',
      spinner: 'bubbles'
    }).then(res => {
      res.dismiss();
    });

    this.listar();
  }

  getout(){
    this.router.navigate(['login']);
  }
  
  modificar(){
    this.router.navigate(['changepass'])
  }

  async listar() {
    let that = this;
    this.loading.create({
      message: 'Obteniendo Información',
      spinner: 'bubbles'
    }).then(async res => {
      res.present();
      let data = await that.api.usuarioObtenerNombre();
      that.user_ini = data['result'];
      console.log(data)
      res.dismiss();
    }); 
  }
  
  logout() {
    localStorage.removeItem('usuario');
    this.router.navigate(['login']);
  }

  async leerQR(){
    
    this.router.navigate(['leer-qr']);
    document.querySelector('body').classList.add('scanner-active');
    await BarcodeScanner.checkPermission({ force: true });
    BarcodeScanner.hideBackground();
  
    const result = await BarcodeScanner.startScan(); 
  
    if (result.hasContent) {9
      console.log(result.content);
      this.texto = result.content;
      this.texto2 = this.texto.split('|')
      this.router.navigate(['principal']);
    }

    document.querySelector('body').classList.remove('scanner-active');
  };

  async AsistenciaAlmacenar(){
    let that = this;
    this.router.navigate(['camara']);
    document.querySelector('body').classList.add('scanner-active');
    await BarcodeScanner.checkPermission({ force: true });
    BarcodeScanner.hideBackground();
    const result = await BarcodeScanner.startScan(); 
    this.loading.create({
      message: 'Registrando asistencia...',
      spinner: 'bubbles'
    }).then(async res => {
      res.present();
      if (result.hasContent) {
        console.log('FSR: '+result.content);
        that.texto = result.content;
        that.texto2 = that.texto.split('|');
        that.router.navigate(['principal']);
        that.asistencia = that.texto2[0]
      }
      console.log('FSR array: '+ that.asistencia);
      let data = await that.api.AsistenciaAlmacenar(
        that.mdl_correo,that.asistencia
      );
      if(data['result'][0].RESPUESTA == 'OK') {
        that.mostrarMensaje('Se ha registrado la asisitencia exitosamente');
        console.log('FSR Result ok: '+data);
      } else if(data['result'][0].RESPUESTA == 'ERR03') {
        that.mostrarMensaje('Ya se regristro su asistencia exitosamente');
        console.log('FSR Resol not: '+ data);
      }else{
        that.mostrarMensaje('FSR resul error: Error al escanear el QR');
      }
      res.dismiss();
    }
    )
    document.querySelector('body').classList.remove('scanner-active');
  };
  async mostrarMensaje(mensaje) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 3000,
      position: 'bottom'
    });

    await toast.present();
  }
}