import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BarcodeScanner } from '@capacitor-community/barcode-scanner';

@Component({
  selector: 'app-leer-qr',
  templateUrl: './leer-qr.page.html',
  styleUrls: ['./leer-qr.page.scss'],
})
export class LeerQrPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  volverPrincipal(){
    document.querySelector('body').classList.remove('scanner-active');
    BarcodeScanner.stopScan(); 
    this.router.navigate(['principal']);
  }

}
