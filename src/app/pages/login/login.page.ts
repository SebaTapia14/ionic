import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiService, correoLogueado, validated } from 'src/app/services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  mdl_mail: string = 'prueba1@gmail.com';
  mdl_password: string = 'prueba1';

  constructor(private router: Router, private api: ApiService, private alertController: AlertController) { }

  ngOnInit() {
  }

  async login() {
    const valido:any = await this.api.maillog(this.mdl_mail, this.mdl_password)
  
    if(valido.result == 'LOGIN OK' ){
        console.log('SNT: LOGUEADO')
        correoLogueado(this.mdl_mail)
        validated(true)
        this.router.navigate(['principal'])        
        this.mdl_mail = ""
        this.mdl_password = ""
    } else if (valido.result == 'LOGIN NO OK' ){
      this.mensaje('los datos de usuario o contraseña son incorrectos')
      console.log('SNT: PROBLEMAS DE LOGIN')
      validated(false)

    }
  }

  registration(){
    this.router.navigate(['registration'])
  }

  async mensaje(msg) {
    const alert = await this.alertController.create({
      header: 'Informacion',
      //subHeader: 'Important message',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }
}
