import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { DbService } from 'src/app/services/db.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  mdl_name: string = '';
  mdl_lastname: string = '';
  mdl_email: string = '';
  mdl_pass: string = '';

  constructor(private router: Router, private api: ApiService, private loading: LoadingController, 
              private toastController: ToastController, private db: DbService) { }

  ngOnInit() {
    this.loading.create({
      message: '',
      spinner: 'bubbles'
    }).then(res => {
      res.dismiss();
    });
  }
  usuarioAlmacenar(){
    let that = this;
    if(this.mdl_pass.length <4){
      that.mostrarMensaje('El minimo de caracteres para la contraseña es 4');
    }else{
      this.loading.create({
        message: 'Creando usuario...',
        spinner: 'bubbles'
      }).then(async res => {
        res.present();
        let data = await that.api.usuarioAlmacenar(
          that.mdl_email, that.mdl_pass, that.mdl_name, that.mdl_lastname
        );
        if(data['result'][0].RESPUESTA == 'OK') {
          console.log(data)
        } else {
          console.log(data)
        }
  
        res.dismiss();
      }
  
      )
    }
  }
  almacenarDb() {
    this.db.validateUser(this.mdl_email).then((data) => {
      if(!data){
        this.db.storeUser(this.mdl_email, this.mdl_pass,this.mdl_name,this.mdl_lastname)
        this.mostrarMensaje('DATOS GUARDADOS CORRECTAMENTE');
      }else{
        this.mostrarMensaje('LOS DATOS NO SE GUARDARON CORRECTAMENTE');
      }
    }
    )
  }
  async mostrarMensaje(mensaje) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 3000,
      position: 'bottom'
    });

    await toast.present();
  }

}
