import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-changepass',
  templateUrl: './changepass.page.html',
  styleUrls: ['./changepass.page.scss'],
})
export class ChangepassPage implements OnInit {


  mdl_email:String = ''
  mdl_newcontra: String = ''
  mdl_oldcontra:String = ''

  constructor(private alertController: AlertController, private router:Router, private loadingCtrl:LoadingController,
              private toastController: ToastController, private api:ApiService) { }

  ngOnInit() {
  }

  modificar(){
    let that = this;
    this.loadingCtrl.create({
      message: 'Cambiando Contraseña...',
      spinner: 'bubbles'
    }).then(async res => {
      res.present();

      let data = await that.api.modpass(this.mdl_email, this.mdl_newcontra, this.mdl_oldcontra);
      console.log('MODIFICAR:',data);
      
      if(data['result'][0].RESPUESTA == 'OK') {
        that.mostrarMensaje('Contraseña Modificada Correctamente.');
        that.limpiar();
      }else if (data['result'][0].RESPUESTA == 'ERR02') {
        that.mostrarMensaje('Contraseña actual no es valida');
      }else {
        that.mostrarMensaje('Error al Cambiar la contraseña');
      }
      res.dismiss();
    });

  }

  async mostrarMensaje(mensaje) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 3000,
      position: 'bottom'
    });

    await toast.present();
  }

  limpiar() {
    this.mdl_email='';
    this.mdl_newcontra='';
    this.mdl_oldcontra='';
  }
}
