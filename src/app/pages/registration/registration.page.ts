import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  mdl_mail: string = '';
  mdl_password:string = '';
  mdl_name: string = '';
  mdl_lastname:string = '';

  constructor(private router: Router, private alertController: AlertController, private api: ApiService) { }

  ngOnInit() {
  }

  async mensaje(msg) {
    const alert = await this.alertController.create({
      header: 'Info',
      //subHeader: 'Important message',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  async register(){
    let emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    if(this.mdl_mail==''){
      this.mensaje('Correo incompleto.')
    }else if(this.mdl_password==''){
      this.mensaje('contraseña inompleta.')
    }else if(!emailRegex.test(this.mdl_mail)){
      this.mensaje('Ingrese un correo válido')
    }else{
      const data:any = await this.api.usuarioAlmacenar(this.mdl_mail, this.mdl_password, this.mdl_name, this.mdl_lastname)
      console.log(data)
      if(data.result[0].RESPUESTA == 'OK'){
        this.mensaje('Usuario registrado con éxito.')
        this.router.navigate(['login'])
      }else if(data.result[0].RESPUESTA == 'ERR01'){
        this.mensaje('Usuario ya registrado.')
      }
    }
  }

  getout(){
    this.router.navigate(['login']);
  }

}
