import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

let correolog: string = '';
let validator: boolean = false;

export let correoLogueado = (arg:string)=>{
  correolog = arg
}

export let validated = (arg:boolean)=>{
  validator = arg
}

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  rutaBase: string = 'https://fer-sepulveda.cl/API_PRUEBA2/api-service.php';

  constructor(private http: HttpClient, private router: Router) { }

  canActivate(){
    if(!validator){
      this.router.navigate(['login']);
      return false;
    } else  {
      return true;
    }
    
  }

  maillog(correo, contrasena) {
    let that = this;
    return new Promise(resolve => {
      resolve(that.http.post(that.rutaBase, {
        nombreFuncion: 'UsuarioLogin',
        parametros:  [correo, contrasena],
      }).toPromise())
    })
  }

  usuarioObtenerNombre() {
    let that = this;
    let url = `${that.rutaBase}?nombreFuncion=UsuarioObtenerNombre&correo=${correolog}`
    return new Promise(resolve => {
      resolve(that.http.get(url).toPromise())
    })
  }

  usuarioAlmacenar(correo, contrasena, nombre, apellido) {
    let that = this;

    return new Promise(resolve => {
      resolve(that.http.post(that.rutaBase, {
        nombreFuncion: 'UsuarioAlmacenar',
        parametros: [correo, contrasena, nombre, apellido]
      }).toPromise())
    })
  }

  modpass(correo, contrasenaNueva, contrasenaActual){
    let that = this;

    return new Promise(resolve => {
      resolve(that.http.patch(that.rutaBase, {
        nombreFuncion: 'UsuarioModificarContrasena',
        parametros: [correo, contrasenaNueva, contrasenaActual]
      }).toPromise())
    })
  }

  AsistenciaAlmacenar(correo, id_clase){
    let that = this;

    return new  Promise(resolve => {
      resolve(that.http.post(that.rutaBase, {
        nombreFuncion: 'AsistenciaAlmacenar',
        parametros: [correo, id_clase]
      }).toPromise())
    })
  }
}
