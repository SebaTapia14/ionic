import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  
  validador: boolean = false;
  mdl_password: string = 'admin';
 

  constructor(private router: Router, private alertController: AlertController,
               private sqlite: SQLite) {
    this.sqlite.create({
      name: 'datos.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('CREATE TABLE IF NOT EXISTS USUARIO(MAIL VARCHAR(75), CONTRASENA VARCHAR (30))',
      []).then(() => {
        console.log('TABLA CREADA OK');
      }).catch(e => {
        console.log('TABLA NO OK')
      })
    }).catch(e => {
      console.log('BASE DE DATOS NO OK')
    })
    }

  canActivate() {
    if(!this.validador){
      this.router.navigate(['login']);
      return false;
    } else  {
      return true;
    }

  }

  storeUser(correo, contrasena, nombre, apellido) {
    this.sqlite.create({
      name: "datos.db",
      iosDatabaseLocation: "default"
    }).then((db: SQLiteObject) => {
      db.executeSql('INSERT INTO PERSONA VALUES(?, ?, ?, ?)', 
      [correo,contrasena, nombre, apellido]).then(() => {
          console.log('SNT: USUARIO ALMACENADO');
      }).catch(e => {
        console.log('SNT: USUARIO NO ALMACENADO')
      })
    }).catch(e => {
      console.log('SNT: BASE DE DATOS OK')
    });
  }
  validateUser(correo){
    return this.sqlite.create({
      name: "datos.db",
      iosDatabaseLocation: "default"
    }).then((db: SQLiteObject) => {
      return db.executeSql('SELECT COUNT(CORREO) AS CANTIDAD FROM USER WHERE CORREO = ?', 
      [correo]).then((data) => {
          if(data.rows.item(0).CANTIDAD == 0){
            return false;
          }
          return true;  
      }).catch(e =>{
        return true;
      })
    }).catch(e => {
      return true;
    });
  }
}

